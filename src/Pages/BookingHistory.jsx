import { useNavigate, Link } from "react-router-dom";
import { authContext } from "../Contexts/AuthContext";
import { useContext, useState, useEffect } from "react";
import Cookies from "js-cookie";
import { Button, Layout, Menu, theme , Collapse  } from "antd";
import {
  HistoryOutlined,
  InfoOutlined,
  HomeOutlined,
  CustomerServiceOutlined,
} from "@ant-design/icons";
import "../Design/BookingHistory.css";

function BookingHistory() {
  const { Content, Sider } = Layout;

  const items = [
    {
      label: <Link to="/auth">Home</Link>,
      icon: <HomeOutlined />,
      key: "Home",
    },
    {
      label: <Link to="/auth/bookingHistory">Booking History</Link>,
      icon: <HistoryOutlined />,
      key: "History",
    },
    {
      label: <Link to="/auth/about">About</Link>,
      icon: <InfoOutlined />,
      key: "About",
    },
    {
      label: <Link to="/auth/contact">Contact</Link>,
      icon: <CustomerServiceOutlined />,
      key: "Contact",
    },
  ];
  const {Panel} = Collapse;
  const { setAuthData } = useContext(authContext);
  const [ booking, setBooking ] = useState([]);
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const navigate = useNavigate();
  const userId = Cookies.get("userId");

  const handleData = async () => {
    try {
      const response = await fetch(
        `http://localhost:3000/booking/all?id=${userId}`
      );
      if (response.ok) {
        const data = await response.json();
        console.log(data);
        setBooking(data);
      } else {
        console.log("Error");
      }
    } catch (error) {
      console.error(error);
    }
  };

  const panels = booking.map(bookings => (
    <Panel header={bookings.date} key={bookings._id}>
        <p>{bookings.status}</p>
    </Panel>
  ))
  const handleLogOut = () => {
    Cookies.remove("authToken");
    setAuthData(null);
    navigate("/login");
  };

  useEffect(() => {
    handleData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div className="booking-page">
      <Layout>
        <Sider
          style={{}}
          collapsible
          collapsed={collapsed}
          onCollapse={(value) => setCollapsed(value)}
        >
          <div style={{ marginTop: "10px" }}>Welcome</div>
          <Menu
            style={{ marginTop: "50px", marginBottom: "50px" }}
            theme="dark"
            defaultSelectedKeys={[""]}
            mode="inline"
            items={items}
          ></Menu>
        </Sider>
        <Layout>
          <Content
            style={{
              margin: "0 16px",
              marginTop: "10px",
            }}
          >
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Button style={{ width: "100px" }} onClick={handleLogOut}>
                Log Out
              </Button>
            </div>
            <div
              style={{
                padding: 24,
                marginTop: "10px",
                boxSizing: "unset",
                minHeight: "90vh",
                display: "flex",
                justifyContent: "flex-start",
                background: colorBgContainer,
              }}
            >
                <Collapse style={{height: '90vh', width:'100%'}}>{panels}</Collapse>
            </div>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
}

export default BookingHistory;
