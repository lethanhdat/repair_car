import { authContext } from "../Contexts/AuthContext";
import { useContext, useState, lazy, Suspense } from "react";
import Cookies from "js-cookie";
import { useNavigate, Link } from "react-router-dom";
import {
  Breadcrumb,
  Button,
  Layout,
  Menu,
  theme,
  Calendar,
  Modal,
  Tooltip,
} from "antd";
import "../Design/MainPage.css";
import {
  HistoryOutlined,
  InfoOutlined,
  HomeOutlined,
  CustomerServiceOutlined,
} from "@ant-design/icons";
import dayjs from "dayjs";
const BookingForm = lazy(() => import("./BookingForm"));

function MainPage() {
  const { Content, Sider } = Layout;

  const items = [
    {
      label: <Link to="/auth">Home</Link>,
      icon: <HomeOutlined />,
      key: "Home",
    },
    {
      label: <Link to="/auth/bookingHistory">Booking History</Link>,
      icon: <HistoryOutlined />,
      key: "History",
    },
    {
      label: <Link to="/auth/about">About</Link>,
      icon: <InfoOutlined />,
      key: "About",
    },
    {
      label: <Link to="/auth/contact">Contact</Link>,
      icon: <CustomerServiceOutlined />,
      key: "Contact",
    },
  ];
  const {auth, setAuthData } = useContext(authContext);
  const navigate = useNavigate();
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const [value, setValue] = useState(() => dayjs(new Date()));
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedDate, setSelectedDate] = useState(null);

  //const userId = Cookies.get("userId");

  // const userId = location.state?.userId;
  const userId = auth.data ? auth.data.userId :null;
  console.log(userId);
  const showPopup = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const onSelect = (newValue, { source }) => {
    if (source === "date") {
      setValue(newValue);
      setSelectedDate(newValue);
      showPopup();
    }
  };

  const onPanelChange = (newValue) => {
    setValue(newValue);
  };

  const disabledDate = (current) => {
    return current < dayjs().startOf("days");
  };

  const handleLogOut = () => {
    Cookies.remove("authToken");
    setAuthData(null);
    navigate("/login");
  };

  return (
    <div className="main-page">
      <Layout>
        <Sider
          style={{}}
          collapsible
          collapsed={collapsed}
          onCollapse={(value) => setCollapsed(value)}
        >
          <div style={{ marginTop: "10px" }}>Welcome</div>
          <Menu
            style={{ marginTop: "50px", marginBottom: "50px" }}
            theme="dark"
            defaultSelectedKeys={[""]}
            mode="inline"
            items={items}
          ></Menu>
        </Sider>
        <Layout>
          <Content
            style={{
              margin: "0 16px",
              marginTop: "10px",
            }}
          >
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <Breadcrumb
                style={{ width: "200px" }}
                items={[
                  { title: "User" },
                  {
                    title: (
                      <Tooltip title={""}>
                        <div
                          style={{
                            maxWidth: "100px",
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            whiteSpace: "nowrap",
                          }}
                        >
                          {""}
                        </div>
                      </Tooltip>
                    ),
                  },
                ]}
              />

              <Button style={{ width: "100px" }} onClick={handleLogOut}>
                Log Out
              </Button>
            </div>
            <div
              style={{
                padding: 24,
                marginTop: "10px",
                boxSizing: "unset",
                minHeight: "90vh",
                background: colorBgContainer,
              }}
            >
              <Calendar
                value={value}
                onSelect={onSelect}
                disabledDate={disabledDate}
                onPanelChange={onPanelChange}
              ></Calendar>
              <Modal
                width={900}
                title="Booking Form"
                destroyOnClose={true}
                open={isModalOpen}
                centered
                onOk={handleOk}
                onCancel={handleCancel}
                cancelButtonProps={{ style: { display: "none" } }}
                okButtonProps={{ style: { display: "none" } }}
              >
                <Suspense>
                  <BookingForm selectedDate={selectedDate} userId={userId} />
                </Suspense>
              </Modal>
            </div>
          </Content>
        </Layout>
      </Layout>
    </div>
  );
}

export default MainPage;
