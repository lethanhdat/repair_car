/* eslint-disable react/prop-types */
import { Space, Button, Avatar, Rate, Spin, Tooltip } from "antd";
import "../Design/BookingForm.css";
import { useEffect, useState } from "react";
import { LoadingOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";

function BookingForm({ selectedDate, userId }) {
  const [activeButton, setActiveButton] = useState(null);
  const [activeMechanic, setActiveMechanic] = useState(null);
  const [hours, setHours] = useState([]);
  const [employees, setEmployees] = useState([]);
  const [isloading, setIsLoading] = useState(false);
  // eslint-disable-next-line no-unused-vars
  const [selectedEmployee, setSelectedEmployee] = useState(null);
  const navigate = useNavigate();

  const getHourData = async () => {
    try {
      const response = await fetch("http://localhost:3000/hour/shifts");
      if (response.ok) {
        const data = await response.json();
        setHours(data);
      }
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    getHourData();
  }, []);

  const handleClick = async (buttonIndex) => {
    setActiveButton(buttonIndex);
    const selectedHour = hours[buttonIndex]._id;
    const date = selectedDate.format("YYYY-MM-DD");
    console.log(userId);
    try {
      setIsLoading(true);
      const response = await fetch(
        `http://localhost:3000/employee/all?date=${date}&hour=${selectedHour}`
      );
      if (response.ok) {
        const data = await response.json();
        console.log(data);
        setTimeout(() => {
          setEmployees(data);
          setIsLoading(false);
        }, 4000);
      }
    } catch (error) {
      console.error(error);
      setIsLoading(false);
    }
  };

  const handleMechanicClick = (mechanicIndex) => {
    setActiveMechanic(mechanicIndex);
    setSelectedEmployee(employees[mechanicIndex]);
    console.log(employees[mechanicIndex]._id);
  };

  const handleSubmit = async () => {
    const selectedHour = hours[activeButton]._id;
    const date = selectedDate.format("YYYY-MM-DD");
    const selectedEmployee = employees[activeMechanic]._id;

    try {
      const response = await fetch(`http://localhost:3000/booking/createP`, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          user_id: userId,
          employee_id: selectedEmployee,
          hours_id: selectedHour,
          date: date,
          status: 1,
        }),
      });
      if (response.ok) {
        const data = await response.json();
        console.log(data);
        setTimeout(() => {
          navigate("/auth/bookingHistory");
        });
      } else {
        console.log("Submit failed");
      }
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <>
      <div style={{ marginBottom: "10px", fontSize: 20 }}>Time</div>
      <div className="form">
        {hours.map((hour, index) => {
          return (
            <Space key={index}>
              <Button
                onClick={() => handleClick(index)}
                style={{
                  backgroundColor: activeButton === index ? "#1677ff" : "white",
                  color: activeButton === index ? "white" : "black",
                  width: 110,
                }}
              >
                {hour.hours}
              </Button>
            </Space>
          );
        })}
      </div>

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          fontSize: 20,
        }}
      >
        <div style={{ marginBottom: "10px" }}>Mechanic</div>
        <div style={{ marginRight: 300 }}> Please choose the time first</div>
      </div>
      {isloading ? (
        <Spin
          indicator={
            <LoadingOutlined
              style={{
                fontSize: 44,
              }}
              spin
            />
          }
        />
      ) : (
        <div
          className="form-mechanic"
          style={{ overflowY: "scroll", maxHeight: "40vh" }}
        >
          {employees.map((employee, index) => {
            return (
              <Space key={index}>
                <Button
                  onClick={() => handleMechanicClick(index)}
                  style={{
                    backgroundColor:
                      activeMechanic === index ? "#1677ff" : "white",
                    color: activeMechanic === index ? "white" : "black",
                    height: 85,
                    width: 320,
                  }}
                >
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <Avatar
                      style={{ height: 65, width: 65, flex: 0.4 }}
                      src={employee.avatar}
                    />
                    <div
                      style={{ flex: 1.6, marginBottom: 10, marginLeft: 10 }}
                    >
                      <Tooltip title={employee.employee_name}>
                        <p
                          style={{
                            maxWidth: "100px",
                            overflow: "hidden",
                            textOverflow: "ellipsis",
                            whiteSpace: "nowrap",
                            marginLeft: 50,
                          }}
                        >
                          {employee.employee_name}
                        </p>
                      </Tooltip>
                      <Rate defaultValue={employee.rating} disabled allowHalf />
                    </div>
                  </div>
                </Button>
              </Space>
            );
          })}
        </div>
      )}

      <div
        style={{ display: "flex", justifyContent: "flex-end", marginTop: 30 }}
      >
        <Button onClick={handleSubmit}>Submit</Button>
      </div>
    </>
  );
}

export default BookingForm;
