import {Button} from 'antd';
import { useNavigate } from 'react-router-dom';
function About() {
    const navigate = useNavigate();

    const goBack = () => {
        navigate('/auth');
    }
    return (
        <>
            <Button onClick={goBack}>Hello</Button>
        </>
    )
}

export default About;