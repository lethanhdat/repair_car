import { useState, useContext, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Button, Form, Input, message, ConfigProvider } from "antd";
import "../Design/Login.css";
import { authContext } from "../Contexts/AuthContext";
import { LockOutlined, UserOutlined } from "@ant-design/icons";
import Cookies from "js-cookie";

function Login() {
  const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const { setAuthData } = useContext(authContext);
  const [messageApi, contextHolder] = message.useMessage();

  useEffect(() => {
    const storeToken = Cookies.get("authToken");
      if (storeToken) {
        setAuthData(storeToken);
          navigate("/auth");
      }
  }, [setAuthData, navigate]);

  const success = () => {
    messageApi.open({
      type: "success",
      content: "Login successfully",
      duration: 2,
    });
  };
  const fail = () => {
    messageApi.open({
      type: "error",
      content: "Login failed",
      duration: 2,
    });
  };

  const issue = () => {
    messageApi.open({
      type: "error",
      content: "Something went wrong with server, try again later",
      duration: 1,
    });
  };

  const handleSubmit = async (values) => {
    try {
      const response = await fetch("http://localhost:3000/user/login", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(values),
      });

      if (response.ok) {
        const data = await response.json();
        const userId = data.users._id;
        const userName = data.users.username;
        Cookies.set("authToken", data.token, { expires: 1 });
        Cookies.set("userId", userId, { expires: 1 });
        console.log(data);
        setAuthData(data.token , userId , userName);
        success();
        setTimeout(() => {
          navigate("/auth");
        },2000);

      } else {
        console.error("Error during login:");
        fail();
      }
    } catch (error) {
      console.error("Error during login:", error);
      issue();
    }
  };

  return (
    <div className="login">
      <div className="login-card">
        {contextHolder}
        <div className="login-container">
          <h1>Login</h1>
          <Form
            name="normal_login"
            className="login-form"
            initialValues={{
              remember: true,
            }}
            autoComplete="off"
            onFinish={handleSubmit}
          >
            <Form.Item
              name="username"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input
                value={username}
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Username"
                onChange={(e) => setUsername(e.target.value)}
              />
            </Form.Item>

            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Item>

            <Form.Item>
              <ConfigProvider
                theme={{
                  components: {
                    Button: {
                      colorPrimary: "red",
                      colorPrimaryHover: "#ff8585",
                    },
                  },
                }}
              >
                <Button type="primary" htmlType="submit">
                  Log in
                </Button>
                <div>
                  Or <Link to="/register">Register</Link>
                </div>
              </ConfigProvider>
            </Form.Item>
          </Form>
        </div>
      </div>
      <div className="image"></div>
    </div>
  );
}

export default Login;
