import  { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { authContext } from '../Contexts/AuthContext';

// eslint-disable-next-line react/prop-types
const ProtectedRoute = ({  children }) => {
  const { auth } = useContext(authContext);
  const {loading}  = auth;

  if (loading) {
    return <div>Loading...</div>;
  }
  
  return (
       auth.data.token ? (
            <>{children}</>
          ) : (
            <Navigate to="/login" />
          )
  );
};

export default ProtectedRoute;