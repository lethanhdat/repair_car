import { useState, useEffect , useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Button, ConfigProvider, Form, Input, message } from "antd";
import "../Design/Register.css";
import { LockOutlined, UserOutlined, PhoneOutlined } from "@ant-design/icons";
import Cookies from "js-cookie";
import { authContext } from "../Contexts/AuthContext";

function Register() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [phone, setPhone] = useState("");
  const [messageApi, contextHolders] = message.useMessage();
  const { setAuthData } = useContext(authContext);
  const navigate = useNavigate();

  useEffect(() => {
    const storeToken = Cookies.get("authToken");
      if (storeToken) {
        setAuthData(storeToken);
          navigate("/auth");
      }
  }, [setAuthData, navigate]);

  const success = () => {
    messageApi.open({
      type: "success",
      content: "Registration successfully",
      duration: 2,
    });
  };

  const fail = () => {
    messageApi.open({
      type: "error",
      content: "Registration failed",
      duration: 2,
    });
  };

  const issue = () => {
    messageApi.open({
      type: "error",
      content: "Something went wrong with server , try again later",
      duration: 1,
    });
  };

  const handleSubmit = async () => {
    try {
      const response = await fetch("http://localhost:3000/user/register", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          username: username,
          password: password,
          phone: phone,
        }),
      });
      if (response.ok) {
        success();
        const data = await response.json();
        console.log(data);
        setTimeout(() => {
          navigate("/login");
        }, 2000);
      } else {
        console.error("Register failed");
        fail();
      }
    } catch (error) {
      console.error(error);
      issue();
    }
  };

  return (
    <div className="register">
      <div className="register-card">
        <div className="register-container">
          {contextHolders}
          <h1>Register</h1>
          <Form
            name="normal_register"
            className="register-form"
            initialValues={{
              remember: true,
            }}
            autoComplete="off"
            onFinish={handleSubmit}
          >
            <Form.Item
              name="username"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                placeholder="Username"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </Form.Item>

            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password
                prefix={<LockOutlined className="site-form-item-icon" />}
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Item>

            <Form.Item
              name="phone"
              rules={[
                {
                  required: true,
                  message: "Please input your phone number!",
                },
              ]}
            >
              <Input
                value={phone}
                onChange={(e) => setPhone(e.target.value)}
                prefix={<PhoneOutlined className="site-form-item-icon" />}
                placeholder="Phone No"
              />
            </Form.Item>

            <Form.Item>
              <ConfigProvider
                theme={{
                  components: {
                    Button: {
                      colorPrimary: "red",
                      colorPrimaryHover: "#ff8585",
                    },
                  },
                }}
              >
                <Button type="primary" htmlType="submit">
                  Register
                </Button>
                <div>
                  Or <Link to="/Login">Login</Link>
                </div>
              </ConfigProvider>
            </Form.Item>
          </Form>
        </div>
      </div>
      <div className="image"></div>
    </div>
  );
}
export default Register;
