import "./App.css";
import { lazy, Suspense } from "react";
import { Routes, Route } from "react-router-dom";
import ProtectedRoute from "./Authentication/ProtectedRoute";
import { BrowserRouter } from "react-router-dom";
const Contact = lazy(() => import('./Pages/Contact'))
const About = lazy(() => import('./Pages/About'))
const BookingHistory = lazy(() => import('./Pages/BookingHistory'));
const MainPage = lazy(() => import('./Pages/MainPage'));
const Login = lazy(() => import('./Authentication/Login'));
const Register = lazy(() => import('./Authentication/Register'));

function App() {
  return (
    <BrowserRouter>
    <Suspense>
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />
        <Route path="/auth" element={<ProtectedRoute><MainPage /></ProtectedRoute>} />
        <Route path="/auth/bookingHistory" element={<ProtectedRoute><BookingHistory /></ProtectedRoute>} />
        <Route path="/auth/about" element={<ProtectedRoute><About /></ProtectedRoute>}/>
        <Route path="/auth/contact" element={<ProtectedRoute><Contact /></ProtectedRoute>}/>
      </Routes>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
