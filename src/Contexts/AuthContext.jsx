import { createContext , useState, useEffect } from "react";
import Cookies from "js-cookie";

// eslint-disable-next-line react-refresh/only-export-components
export const authContext = createContext({});

// eslint-disable-next-line react/prop-types
const AuthProvider = ({ children }) => {
  const [auth, setAuth] = useState({ loading: true, data: { token: null, userId: null } });

  const setAuthData = (token, userId) => {
    setAuth({ ...auth, data: { token, userId } });
  };

  useEffect(() => {
    const authDataStr = Cookies.get("authToken") ;
    if (authDataStr ) {
      setAuth({ loading: false, data: { token: authDataStr, userId: Cookies.get("userId") } });
    } else {
      setAuth({ loading: false, data: { token: null, userId: null } });
    }
  }, []);
  
  useEffect(() => {
    if (auth.data && auth.data.token) {
      Cookies.set("authToken", auth.data.token);
    }
  }, [auth.data]);

  return (
    <authContext.Provider value={{ auth, setAuthData }}>
      {children}
    </authContext.Provider>
  );
};

export default AuthProvider;